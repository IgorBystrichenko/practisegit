package com.project.project1;
/*
SOME
GOOD
COMMENTARY
*/
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.*;

public class MainActivity extends AppCompatActivity {
    public static final String LINK = "LINK";
    public static final String LOGIN = "LOGIN";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String[] logins = getResources().getStringArray(R.array.Logins);
        String[] passwords = getResources().getStringArray(R.array.Passwords);
        String[] links = getResources().getStringArray(R.array.Links);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button1).setOnClickListener(v -> {
            String login = ((EditText)findViewById(R.id.editT1)).getText().toString();
            String password = ((EditText)findViewById(R.id.editT2)).getText().toString();
            boolean flag = true;
            for(int i = 0; i < logins.length && flag; i++)
                if ((login.compareTo(logins[i]) == 0) && (password.compareTo(passwords[i]) == 0)) {
                    Intent intent = new Intent(this, WebActivity.class);
                    intent.putExtra("LINK", links[i]);
                    intent.putExtra("LOGIN", logins[i]);
                    startActivity(intent);
                    flag = false;
                }
            if(flag)
                ((TextView)findViewById(R.id.wrongpswd)).setText("Вы введи неверный логин или пароль");
        });
    }
}
